from collections import defaultdict
import numpy as np
import imageio
import os

from . import parse_args, Database, Color, Edge, HOG, Daisy, FeatureFusion
from .utils import plot_saves
from prepare_data import prepare_data


def evaluate_Klass(Klass, save=True):
    """
        Fonction qui permet d'évaluer, et faire une prédiction du jeu de test.

        Paramètres :
        ------------
         - Klass (Color|Edge|HOG|Daisy|FeatureFusion): La classe qui est utilisé.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """

    validation = Database("validation")  # validation database instance
    train = Database("train")  # train database instance
    test = Database("test")  # test database instance

    # exécute la fonction de prédiction
    aps, preds = Klass.evaluate(train, test)

    # Affiche la précision de la prédiction
    cls_MAPs = []
    for cls_, cls_APs in aps.items():
        MAP = np.mean(cls_APs)
        print("Class {}, MAP {}".format(cls_, MAP))
        cls_MAPs.append(MAP)
    print("MMAP", np.mean(cls_MAPs))

    # sauvegarde des fichiers dans le dossier result
    saves = defaultdict(list)
    for i in range(len(test)):

        bid = imageio.imread(test.data.img[i])
        if save == True:
            imageio.imsave(
                os.path.join("dataset", "result",
                             preds[i], test.get_name(i)), bid
            )
        saves[preds[i]].append(bid)

    return saves


def color(plot=False, save=True):
    """
        Point d'entré du programme pour 'color'.

        Paramètres :
        ------------
         - plot (bool) = False: affiche un échantillon des classes prédites.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - la classe ::class::`Color`.
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """
    color = Color(depth=3, h_type="region", d_type="d1", n_bin=12, n_slice=3)
    saves = evaluate_Klass(color, save=save)

    if plot:
        plot_saves(saves)

    return color, saves


def edge(plot=False, save=True):
    """
        Point d'entré du programme pour 'edge'.

        Paramètres :
        ------------
         - plot (bool) = False: affiche un échantillon des classes prédites.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - la classe ::class::`Edge`.
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """
    edge = Edge(depth=1, h_type="global",
                d_type="d1", stride=(1, 1), n_slice=5)
    saves = evaluate_Klass(edge, save=save)

    if plot:
        plot_saves(saves)

    return edge, saves


def hog(plot=False, save=True):
    """
        Point d'entré du programme pour 'hog'.

        Paramètres :
        ------------
         - plot (bool) = False: affiche un échantillon des classes prédites.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - la classe ::class::`HOG`.
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """
    hog = HOG(depth=3, h_type="global", d_type="d1", n_slice=2, n_bin=10)
    saves = evaluate_Klass(hog, save=save)

    if plot:
        plot_saves(saves)

    return hog, saves


def daisy(plot=False, save=True):
    """
        Point d'entré du programme pour 'daisy'.

        Paramètres :
        ------------
         - plot (bool) = False: affiche un échantillon des classes prédites.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - la classe ::class::`Daisy`.
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """
    daisy = Daisy(depth=3, h_type="global", d_type="d1", n_slice=2)
    saves = evaluate_Klass(daisy, save=save)

    if plot:
        plot_saves(saves)

    return daisy, saves


def fusion(plot=False, save=True):
    """
        Point d'entré du programme pour 'fusion'.

        Paramètres :
        ------------
         - plot (bool) = False: affiche un échantillon des classes prédites.
         - save (bool) = True: sauvegarde les images prédites dans le dossier 'results'.

        Retour :
        --------
         - la classe ::class::`FeatureFusion`.
         - le dictionnaire contenant pour chaque classes prédites, les images.
           .. note: les images sont au format PIL
    """
    fusion = FeatureFusion(
        features=[color(save=False)[0], daisy(save=False)[0]], depth=1, d_type="d1")
    saves = evaluate_Klass(fusion, save=save)

    if plot:
        plot_saves(saves)

    return fusion, saves


def main():
    """
        Point d'entré du programme principal.

        Exemples :
        ----------
        - `python -m attribut_classification -h` : affiche l'aide.
        - `python -m attribut_classification dataset`: prépare le jeu de données.
    """
    args = parse_args()
    if args.command == "dataset":
        prepare_data()
    elif args.command == "color":
        color(plot=args.plot)
    elif args.command == "edge":
        edge(plot=args.plot)
    elif args.command == "daisy":
        daisy(plot=args.plot)
    elif args.command == "hog":
        hog(plot=args.plot)
    elif args.command == "fusion":
        fusion(plot=args.plot)


if __name__ == "__main__":
    main()
