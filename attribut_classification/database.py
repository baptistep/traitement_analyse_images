import pandas as pd
import os
import shutil

from settings import DATASET_PATH


DATASET_TEMP = os.path.join(DATASET_PATH, "temp")
DATASET_CACHE = os.path.join(DATASET_PATH, "cache")
DATASET_RESULT = os.path.join(DATASET_PATH, "result")


class Database:
    def __init__(self, fdata="train"):
        # make directories
        os.makedirs(DATASET_TEMP, exist_ok=True)
        os.makedirs(DATASET_CACHE, exist_ok=True)
        shutil.rmtree(DATASET_RESULT, ignore_errors=True)
        os.makedirs(DATASET_RESULT, exist_ok=True)

        self._gen_csv(fdata)

        # save params
        self.data = pd.read_csv(os.path.join(DATASET_TEMP, "%s.csv" % fdata))
        self.classes = set(self.data["cls"])
        self.fdata = fdata

        for c in self.classes:
            os.makedirs(os.path.join(DATASET_RESULT, c), exist_ok=True)

    def _gen_csv(self, fdata):
        file = "%s/%s.csv" % (DATASET_TEMP, fdata)
        data = "%s/%s" % (DATASET_PATH, fdata)

        with open(file, "w", encoding="UTF-8") as f:
            f.write("img,cls")
            _, features, _ = next(os.walk(data))
            for feature in features:
                for _, _, files in os.walk(os.path.join(data, feature)):
                    for name in files:
                        if not name.endswith(".jpg"):
                            continue
                        img = os.path.join(data, feature, name)
                        f.write("\n%s,%s" % (img, feature))

    def __len__(self):
        return len(self.data)

    def get_data(self):
        return self.data

    def get_class(self):
        return self.classes

    def get_name(self, id):
        return self.data.loc[id, "img"].replace("\\", "/").split("/")[-1]

    def get_type(self):
        return self.fdata
