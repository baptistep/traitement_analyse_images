# -*- coding: utf-8 -*-

from sklearn.utils.extmath import weighted_mode
from scipy import spatial
import numpy as np


class Metric:
    """ Some static functions useful for Evaluation."""

    @classmethod
    def infer(cls, query, samples, depth=None, d_type="d1"):
        """Infer a query, return it's ap."""

        results = []
        # Compute the distance between the query and the samples
        # aka. Compare the query and the train dataset.
        for sample in samples:
            # prevent the way that the query is inside the samples
            if query["img"] == sample["img"]:
                continue

            results.append(
                {
                    "dis": cls.distance(
                        query["hist"], sample["hist"], d_type=d_type
                    ),
                    "cls": sample["cls"],
                    "img": sample["img"],
                }
            )

        # Sort results to have lowest first
        results = sorted(results, key=lambda x: x["dis"])
        # Keep only the {depth} values of results if defined else results
        res = results[:depth] if depth and depth < len(
            results) else results

        # Find the prediction with weighted mode.
        pred = np.array_str(
            weighted_mode(
                [sub["cls"] for sub in res],
                np.reciprocal([sub["dis"] for sub in res]),
            )[0]
        )[2:-2]

        # evaluate if the most of results that they have the lowest distance is the query label.
        ap = cls.AP(query["cls"], res)

        return ap, pred

    @staticmethod
    def AP(label, results) -> bool:
        """ Infer if upper thant 50 % are the class of the result."""
        hits = len(
            list(filter(lambda result: result["cls"] == label, results)))
        return (hits / len(results)) > 0.5

    @staticmethod
    def distance(v1, v2, d_type="d1"):
        assert v1.shape == v2.shape, "shape of two vectors need to be same!"

        if d_type == "d1":
            return np.sum(np.absolute(v1 - v2))
        elif d_type == "d2":
            return np.sum((v1 - v2) ** 2)
        elif d_type == "cosine":
            return spatial.distance.cosine(v1, v2)
        elif d_type == "square":
            return np.sum((v1 - v2) ** 2)
        elif d_type in ["d2-norm", "d7", "d8"]:
            return 2 - 2 * np.dot(v1, v2)
        elif d_type in ["d3", "d4", "d5", "d6"]:
            pass

        raise NotImplementedError(
            f"Actually, the d_type {d_type} is not implemented!"
        )


class Evaluation:
    """Evaluation Interface."""

    metric = Metric()

    def make_samples(self, db):
        raise NotImplementedError("Needs to implemented this method")

    def evaluate(self, train, test):
        depth = self.depth
        d_type = self.d_type

        classes = train.get_class()
        ret = {c: [] for c in classes}
        preds = []

        train_samples = self.make_samples(train)
        test_samples = self.make_samples(test)

        for sample in test_samples:
            ap, pred = self.metric.infer(
                sample, samples=train_samples, depth=depth, d_type=d_type
            )

            ret[sample["cls"]].append(ap)
            preds.append(pred)

        return ret, preds
