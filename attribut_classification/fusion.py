# -*- coding: utf-8 -*-

from .evaluate import Evaluation
from .database import Database

from .color import Color
from .edge import Edge

import numpy as np
import itertools
import os

feat_pools = ["color", "daisy", "edge", "gabor", "hog", "vgg", "res"]


class FeatureFusion(Evaluation):
    def __init__(self, features, depth=1, d_type="d1"):
        assert len(features) > 1, "need to fuse more than one feature!"
        self.features = features
        self.samples = None
        self.depth = depth
        self.d_type = d_type

    def make_samples(self, db, verbose=False):
        if verbose:
            print("Use features {}".format(" & ".join(self.features)))

        if self.samples == None:
            feats = []
            for f_class in self.features:
                feats.append(f_class.make_samples(db))
            samples = self._concat_feat(db, feats)
            self.samples = samples  # cache the result
        return self.samples

    def _concat_feat(self, db, feats):
        samples = feats[0]
        delete_idx = []
        for idx in range(len(samples)):
            for feat in feats[1:]:
                feat = self._to_dict(feat)
                key = samples[idx]["img"]
                if key not in feat:
                    delete_idx.append(idx)
                    continue
                assert feat[key]["cls"] == samples[idx]["cls"]
                samples[idx]["hist"] = np.append(
                    samples[idx]["hist"], feat[key]["hist"]
                )
        for d_idx in sorted(set(delete_idx), reverse=True):
            del samples[d_idx]
        if delete_idx != []:
            print("Ignore %d samples" % len(set(delete_idx)))

        return samples

    def _to_dict(self, feat):
        ret = {}
        for f in feat:
            ret[f["img"]] = {"cls": f["cls"], "hist": f["hist"]}
        return ret
