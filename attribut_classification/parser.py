import argparse
import sys


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # prepare dataset
    prepare_parser = subparsers.add_parser(
        "dataset", help="split features as specified inside the settings file."
    )
    prepare_parser.set_defaults(command="dataset")
    # prepare_parser.add_argument()

    # about color classification
    color_parser = subparsers.add_parser(
        "color", help="start color classification.")
    color_parser.set_defaults(command="color")
    color_parser.add_argument('--plot', action="store_true", default=False,
                              help="afficher un échantillon d'image classifié")

    # about edge classification
    edge_parser = subparsers.add_parser(
        "edge", help="start edge classification.")
    edge_parser.set_defaults(command="edge")
    edge_parser.add_argument('--plot', action="store_true", default=False,
                             help="afficher un échantillon d'image classifié")

    # about daisy classification
    daisy_parser = subparsers.add_parser(
        "daisy", help="start daisy classification.")
    daisy_parser.set_defaults(command="daisy")
    daisy_parser.add_argument('--plot', action="store_true", default=False,
                              help="afficher un échantillon d'image classifié")

    # about hog classification
    hog_parser = subparsers.add_parser(
        "hog", help="start hog classification.")
    hog_parser.set_defaults(command="hog")
    hog_parser.add_argument('--plot', action="store_true", default=False,
                            help="afficher un échantillon d'image classifié")

    # about fusion (color and edge) classification
    fusion_parser = subparsers.add_parser(
        "fusion", help="start fusion (color and edge) classification."
    )
    fusion_parser.set_defaults(command="fusion")
    fusion_parser.add_argument('--plot', action="store_true", default=False,
                               help="afficher un échantillon d'image classifié")

    args = parser.parse_args()
    if getattr(args, "command", None) is None:
        parser.print_help()
        sys.exit(2)
    return args
