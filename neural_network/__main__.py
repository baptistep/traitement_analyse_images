import os
import tensorflow as tf

from tensorflow.keras.preprocessing.image import ImageDataGenerator

from . import parse_args, get_or_train_model, evaluate_model
from prepare_data import prepare_data
from settings import DATASET_PATH, DATASET_FEATURES

# setup variables for training our model
batch_size = 5
epochs = 15
IMG_HEIGHT = 150
IMG_WIDTH = 150


def train(features=DATASET_FEATURES):

    # Format the images into appropriately pre-processed floating point tensors before feeding to the network
    train_datagen = ImageDataGenerator(
        rescale=1.0 / 255,  # rescaling to have 0-1 values
    )
    train_datagen = train_datagen.flow_from_directory(
        batch_size=batch_size,
        directory=os.path.join(DATASET_PATH, "train"),
        shuffle=True,
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        class_mode="sparse",
    )

    validate_datagen = ImageDataGenerator(rescale=1.0 / 255)
    validate_datagen = validate_datagen.flow_from_directory(
        batch_size=batch_size,
        directory=os.path.join(DATASET_PATH, "validation"),
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        class_mode="sparse",
    )

    STEP_SIZE_TRAIN = train_datagen.n//train_datagen.batch_size
    STEP_SIZE_VALID = validate_datagen.n//validate_datagen.batch_size
    model, history = get_or_train_model(
        input_shape=(IMG_HEIGHT, IMG_WIDTH, 3),
        save_filename="_".join(features),
        save_dirpath=os.path.join(DATASET_PATH, "models"),
        epochs=epochs,
        train_datagen=train_datagen,
        steps_per_epoch=STEP_SIZE_TRAIN,
        validate_datagen=validate_datagen,
        validation_steps=STEP_SIZE_VALID,
        output_len=len(features)
    )

    return model, history


def test(batch_size, features=DATASET_FEATURES):

    test_datagen = ImageDataGenerator(rescale=1.0 / 255)
    test_datagen = test_datagen.flow_from_directory(
        directory=os.path.join(DATASET_PATH, "test"),
        target_size=(IMG_HEIGHT, IMG_WIDTH),
        class_mode="sparse",
        shuffle=False
    )

    model, _ = get_or_train_model(
        input_shape=(IMG_HEIGHT, IMG_WIDTH, 3),
        save_filename="_".join(features),
        save_dirpath=os.path.join(DATASET_PATH, "models"),
        epochs=epochs,
        train_datagen=None,
        steps_per_epoch=None,
        validate_datagen=None,
        validation_steps=None,
        output_len=len(features)
    )

    return evaluate_model(test_datagen, model)


def main():
    args = parse_args()
    if args.command == "dataset":
        prepare_data()
    elif args.command == "train":
        train()
    elif args.command == "test":
        test(args.maxi)


if __name__ == "__main__":
    main()
