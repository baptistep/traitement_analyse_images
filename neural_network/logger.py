from enum import IntEnum


class LogLevel(IntEnum):
    DEBUG = 10
    INFO = 20
    WARN = 30
    ERROR = 40


class Logger:
    def __init__(self, level=LogLevel.INFO, debug=True):
        self._debug = debug
        self.level = LogLevel(level)

    def log(self, level, message):
        level_name = LogLevel(level).name
        message = ("[%s]" % level_name) + "\t" + " ".join(message)
        if self._debug and level >= self.level:
            print(message)

    def debug(self, *message):
        self.log(LogLevel.DEBUG, message)

    def info(self, *message):
        self.log(LogLevel.INFO, message)

    def warning(self, *message):
        self.log(LogLevel.WARN, message)

    def error(self, *message):
        self.log(LogLevel.ERROR, message)


logger = Logger(debug=True)

if __name__ == "__main__":
    logger.debug("coucou")
    logger.info("coucou")
    logger.warning("coucou")
    logger.error("coucou")
