import os
from tensorflow.keras import models, layers, callbacks


def get_model(input_shape, output_len):
    model = models.Sequential()

    model.add(layers.Conv2D(
        32, (3, 3), activation="relu", input_shape=input_shape))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Dropout(0.2))

    model.add(layers.Conv2D(64, (3, 3), activation="relu"))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation="relu"))
    model.add(layers.Dropout(0.2))

    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation="relu"))
    model.add(layers.Dense(output_len, activation="softmax"))

    model.compile(
        optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"]
    )

    return model


def get_or_train_model(
    input_shape,
    save_dirpath,
    save_filename,
    epochs,
    train_datagen,
    steps_per_epoch,
    validate_datagen,
    validation_steps,
    output_len,
    load=True,
    save=True,
):
    """
    """
    history = None

    # load our model
    model = get_model(input_shape, output_len)

    if os.path.isfile(f"{save_dirpath}/{save_filename}.h5") and load:
        # load weights into the model
        model.load_weights(f"{save_dirpath}/{save_filename}.h5")

    else:
        # fit the model
        history = model.fit(
            train_datagen,
            epochs=epochs,
            steps_per_epoch=steps_per_epoch,
            validation_data=validate_datagen,
            validation_steps=validation_steps,
            callbacks=[callbacks.EarlyStopping(
                monitor="val_loss", patience=3)],
        )
        if save:
            model.save_weights(f"{save_dirpath}/{save_filename}.h5")

    return model, history
