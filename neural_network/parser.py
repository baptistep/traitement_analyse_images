import argparse
import sys


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # prepare dataset
    prepare_parser = subparsers.add_parser(
        "dataset", help="split features as specified inside the settings file."
    )
    prepare_parser.set_defaults(command="dataset")
    # prepare_parser.add_argument()

    # start learning
    learn_parser = subparsers.add_parser(
        "train", help="train a model based on dataset")
    learn_parser.set_defaults(command="train")
    # learn_parser.add_argument('')

    # start testing
    test_parser = subparsers.add_parser(
        "test", help="test a model based on dataset")
    test_parser.set_defaults(command="test")
    test_parser.add_argument(
        "--maxi", type=int, default=5, help="number of max feature to show."
    )

    args = parser.parse_args()
    if getattr(args, "command", None) is None:
        parser.print_help()
        sys.exit(2)
    return args
