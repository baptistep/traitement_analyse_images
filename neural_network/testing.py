import os
import imageio
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


def evaluate_model(test_datagen, model):

    # evaluate the model
    STEP_SIZE_TEST = test_datagen.n//test_datagen.batch_size
    results = model.evaluate(test_datagen, steps=STEP_SIZE_TEST)
    print("test loss, test acc:", results)

    filenames = test_datagen.filenames
    y_true = []
    y_pred = []

    test_datagen.reset()
    pred = model.predict(test_datagen, steps=STEP_SIZE_TEST)
    predicted_class_indices = np.argmax(pred, axis=1)

    y_pred = []
    test_datagen.reset()
    for i in range(1, len(iter(test_datagen))):
        features, labels = next(iter(test_datagen))
        y_pred += list(labels.astype(int))

    #
    labels = (test_datagen.class_indices)
    labels = dict((v, k) for k, v in labels.items())

    predictions = [labels[k] for k in predicted_class_indices]
    true_values = [labels[k] for k in y_pred]

    plot_confusion = ConfusionMatrixDisplay(
        confusion_matrix=confusion_matrix(true_values, predictions),
        display_labels=[v for _, v in labels.items()]
    )
    plot_confusion.plot()
    plt.show()

    return predictions, filenames[:len(predictions)]


def saves_pred(predictions, filenames, save=False):

    assert len(predictions) == len(
        filenames), f'{len(predictions)} != {len(filenames)}'

    saves = defaultdict(list)
    for i, filename in enumerate(filenames):

        bid = imageio.imread(os.path.join(
            "dataset", "test", filename
        ))
        if save == True:
            imageio.imsave(
                os.path.join(
                    "dataset", "result",
                    predictions[i],
                    filename.split(os.sep)[-1]
                ), bid
            )
        saves[predictions[i]].append(bid)

    return saves
