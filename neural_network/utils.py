import numpy as np
import matplotlib.pyplot as plt


def plot_saves(saves):

    w = 15
    h = 15
    fig = plt.figure(figsize=[w, h])
    columns = 4
    rows = 5

    n = 0
    for r in range(1, rows):
        for i, k in enumerate(saves.keys()):
            if i < columns:
                n += 1

                index = np.random.randint(0, high=len(saves[k]))
                img = saves[k][index]
                fig.add_subplot(rows, columns, n)
                plt.title(k)
                plt.imshow(img, cmap="gray")

    plt.show()
