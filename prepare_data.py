"""
Preparation data file.

- select image theme
- split in 3 dataset (60%/20%/20%)
"""
import os
import shutil
import random
import tqdm

from settings import DATABASE_FEATURES, DATABASE_PATH


def copy_file_to_dataset(
    database=DATABASE_PATH, features=DATABASE_FEATURES, augmented=False
):
    for feature in tqdm.tqdm(features):
        for root, _, files in os.walk(os.path.join(database, feature)):
            # prepare lenght split
            max_train, max_test, max_validation = (
                len(files) * 0.6,
                len(files) * 0.2,
                len(files) * 0.2,
            )

            random.shuffle(files)  # randomize dataset
            for i, file in enumerate(files):
                if ".jpg" in file or ".png" in file:
                    if i < max_train:
                        pathdest = os.path.join(
                            "dataset", "train", root.split(os.sep)[-1]
                        )
                        shutil.copy(os.path.join(root, file), pathdest)
                    elif i < (max_train + max_test):
                        pathdest = os.path.join(
                            "dataset", "test", root.split(os.sep)[-1]
                        )
                        shutil.copy(os.path.join(root, file), pathdest)
                    else:
                        pathdest = os.path.join(
                            "dataset", "validation", root.split(os.sep)[-1]
                        )
                        shutil.copy(os.path.join(root, file), pathdest)


def create_dataset_folders(rmdirs=False, features=DATABASE_FEATURES):
    if rmdirs:
        shutil.rmtree(os.path.join("dataset", "train"), ignore_errors=True)
        shutil.rmtree(os.path.join("dataset", "test"), ignore_errors=True)
        shutil.rmtree(os.path.join("dataset", "validation"),
                      ignore_errors=True)
        shutil.rmtree(os.path.join("dataset", "cache"), ignore_errors=True)
        shutil.rmtree(os.path.join("dataset", "result"), ignore_errors=True)
        shutil.rmtree(os.path.join("dataset", "temp"), ignore_errors=True)

    for feature in features:
        os.makedirs(os.path.join("dataset", "train", feature), exist_ok=True)
        os.makedirs(os.path.join("dataset", "test", feature), exist_ok=True)
        os.makedirs(os.path.join(
            "dataset", "validation", feature), exist_ok=True)
        os.makedirs(os.path.join("dataset", "models"), exist_ok=True)


def prepare_data(features=DATABASE_FEATURES):
    create_dataset_folders(features=features, rmdirs=True)
    copy_file_to_dataset(features=features)


if __name__ == "__main__":
    prepare_data()
