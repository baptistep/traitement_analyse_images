# Image processing and analysis 

![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.png?v=103)


## Command list

* about Classification
  - `python -m attribut_classification -h`: show the help of commands.
  - `python -m attribut_classification dataset`: generate the dataset of features specified inside [settings.py](settings.py).
  - `python -m attribut_classification color`: predict and evaluate features by using color.
  - `python -m attribut_classification edge`: predict and evaluate features by using edge.
  - `python -m attribut_classification daisy`: predict and evaluate features by using daisy.
  - `python -m attribut_classification hog`: predict and evaluate features by using HOG.
  - `python -m attribut_classification fusion`: predict and evaluate features by using colors and daisy.

* about NN
  - `python -m neural_network -h`: show the help of commands.
  - `python -m neural_network dataset`: generate the dataset of features specified inside [settings.py](settings.py).
  - `python -m neural_network train`: fit and save the model of those features.
  - `python -m neural_network test --maxi 5`: show the prediction of the 5 first test features. 

## Author

Baptiste PRIEUR <<bprieur@enssat.fr>>
