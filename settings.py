"""
Fichiers de configuration. 

Localiser une base de données, sélectionner les images à analyser.
"""
import os

DATABASE_PATH = os.path.join(os.path.abspath('../database'), "CorelDB")
DATABASE_FEATURES = [
    "art_cybr",
    "eat_drinks",
    "obj_balloon",
    "obj_doll"
]
DATABASE_FEATURES.sort()

DATASET_PATH = os.path.join(os.getcwd(), "dataset")
DATASET_FEATURES = DATABASE_FEATURES
